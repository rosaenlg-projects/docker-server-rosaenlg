# Docker Server for RosaeNLG

Based on RosaeNLG Java Server. For documentation:
- [RosaeNLG main doc](https://rosaenlg.org)
- [RosaeNLG Java Server doc](https://gitlab.com/rosaenlg-projects/rosaenlg-java-server)

You can test using `test.sh`.

